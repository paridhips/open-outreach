import {
  Button,
  Pressable,
  StyleSheet,
  Text,
  TextInput,
  View,
} from "react-native";
import { router } from "expo-router";
import { MaterialIcons } from "@expo/vector-icons";
export default function LoginPage() {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Log In</Text>
      <View style={styles.form}>
        <View style={styles.inputContainer}>
          <MaterialIcons style={{ paddingLeft: 16 }} name="email" size={18} />
          <TextInput inputMode="email" placeholder="Email" />
        </View>
        <View style={styles.inputContainer}>
          <MaterialIcons
            style={{ paddingLeft: 16 }}
            name="password"
            size={18}
          />
          <TextInput
            inputMode="text"
            secureTextEntry={true}
            placeholder="Password"
          />
        </View>
        <View style={styles.optionsBar}>
          <Pressable onPress={()=>router.push("/signUp")}>
            <Text style={styles.optionsText}>Create An Account</Text>
          </Pressable>
          <Pressable>
            <Text style={styles.optionsText}>Forgot Password?</Text>
          </Pressable>
        </View>
        <Pressable
          onPress={() => router.replace("/(home)")}
          style={styles.button}
        >
          <Text style={styles.buttonText}>Log In</Text>
        </Pressable>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 32,
    verticalAlign: "middle",
    gap: 16,
    justifyContent: "center",
  },
  form: { gap: 16 },
  inputContainer: {
    flexDirection: "row",
    alignItems: "center",
    gap: 10,
    width: "auto",
    backgroundColor: "gainsboro",
    borderRadius: 32,
    height: 45,
    padding: 4,
  },
  textInput: {
    fontSize: 20,
  },
  title: {
    fontSize: 48,
    fontWeight: "bold",
    textAlign: "center",
  },
  button: {
    backgroundColor: "black",
    padding: 12,
    borderRadius: 32,
  },
  buttonText: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
    fontSize: 16,
  },
  optionsBar: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  optionsText: {
    fontWeight: "bold",
    color: "#3572ef",
  },
});
